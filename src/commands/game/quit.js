const Player = require('../../models/Player');

module.exports = {
    name: 'quit',
    description: 'End the current adventure!',
    testOnly: false,
    callback: async (client, interaction) => {
        const userId = interaction.member.id;
        const player = await Player.findOne({ userId });

        if (!player) {
            return interaction.reply("You haven't started an adventure yet!");
        }

        if (!player.battleState) {
            return interaction.reply("You're not currently in a battle!");
        }

        player.battleState = false;
        player.hitPoints = player.maxHitPoints;  // Replenishing health to max
        player.energy = player.maxEnergy;  // Replenishing energy (MP) to max
        await player.save();

        return interaction.reply("You have ended your adventure. All your stats have been replenished!");
    },
};
