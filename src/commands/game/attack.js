const Player = require('../../models/Player');
const Enemy = require('../../models/Enemy');
const Level = require('../../models/Level');

module.exports = {
    name: 'atk',
    description: 'Attack the AI during your adventure!',
    testOnly: false,
    callback: async (client, interaction) => {
        const userId = interaction.member.id;
        const player = await Player.findOne({ userId });
        const levelInfo = await Level.findOne({ userId });
        const enemy = await Enemy.findOne({ userId });

        if (!player || !player.battleState) {
            return interaction.reply("You're not currently in a battle! Use `/adv` to start an adventure.");
        }

        const levelBonus = levelInfo ? levelInfo.level * 5 : 0;
        const totalPlayerAttack = player.attackRating + levelBonus;

        const playerAttack = Math.floor(Math.random() * (totalPlayerAttack + 1));
        const aiAttack = Math.floor(Math.random() * (enemy.attackRating + 1));

        if (Math.random() < player.hitAccuracy) {
            enemy.hitPoints -= playerAttack;
            await enemy.save();
        }

        if (enemy.hitPoints <= 0) {
            player.battleState = false;
            player.hitPoints = player.maxHitPoints;
            player.energy = player.maxEnergy;
            await player.save();
            return interaction.reply("You defeated the AI! Your adventure ends here. Use `/adv` to start a new one.");
        }

        player.hitPoints -= aiAttack;
        if (player.hitPoints <= 0) {
            player.battleState = false;
            player.hitPoints = player.maxHitPoints;
            player.energy = player.maxEnergy;
            await player.save();
            return interaction.reply("You were defeated by the AI! Your adventure has ended. Use `/adv` to try again.");
        }

        await player.save();
        return interaction.reply(`You attacked the AI for ${playerAttack} damage. The AI attacked you for ${aiAttack} damage. Your HP: ${player.hitPoints.toFixed(2)} | AI's HP: ${enemy.hitPoints.toFixed(2)} | Your MP: ${player.energy}`);
    },
};
