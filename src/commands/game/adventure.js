const Player = require('../../models/Player');
const Enemy = require('../../models/Enemy');

module.exports = {
    name: 'adv',
    description: 'Start an adventure and battle against an AI!',
    testOnly: false,
    callback: async (client, interaction) => {
        const userId = interaction.member.id;
        let player = await Player.findOne({ userId });
        let enemy = await Enemy.findOne({ userId });

        // Initialize player if not exists
        if (!player) {
            const newPlayer = new Player({
                userId,
                speed: Math.floor(Math.random() * 21) // Random speed between 0 and 20
            });
            await newPlayer.save();
            player = newPlayer;
        }

        // Initialize enemy if not exists
        if (!enemy) {
            const newEnemy = new Enemy({
                userId,
                speed: Math.floor(Math.random() * 21) // Random speed between 0 and 20
            });
            await newEnemy.save();
            enemy = newEnemy;
        }

        // If battleState is false, initialize a new battle
        if (!player.battleState) {
            player.battleState = true;

            // Validate that enemy.maxHitPoints is a number
            if (typeof enemy.maxHitPoints !== 'number' || isNaN(enemy.maxHitPoints)) {
                enemy.maxHitPoints = 100;  // Default value
            }
            enemy.hitPoints = enemy.maxHitPoints; // Reset the AI's health

            await player.save();
            await enemy.save();

            // Determine who attacks first based on speed
            if (player.speed > enemy.speed) {
                return interaction.reply(`Your adventure begins! The battle is on! Your MP: ${player.energy} | Your Speed: ${player.speed} | AI's Speed: ${enemy.speed}. You have the first move due to your higher speed. Use /atk to attack!`);
            } else {
                return interaction.reply(`Your adventure begins! The battle is on! Your MP: ${player.energy} | Your Speed: ${player.speed} | AI's Speed: ${enemy.speed}. The AI has the first move due to its higher speed. Defend yourself!`);
            }
        } else {
            // If player is already in a battleState
            return interaction.reply(`You are already in a battle! Your MP: ${player.energy} | Your Speed: ${player.speed} | AI's Speed: ${enemy.speed}. Use /atk to continue attacking.`);
        }
    },
};
