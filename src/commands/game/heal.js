const Player = require('../../models/Player');
const Enemy = require('../../models/Enemy');

module.exports = {
    name: 'heal',
    description: 'Heal yourself during a battle!',
    testOnly: false,
    callback: async (client, interaction) => {
        const userId = interaction.member.id;
        const player = await Player.findOne({ userId });
        const enemy = await Enemy.findOne({ userId });

        if (!player || !player.battleState) {
            return interaction.reply('You are not in a battle!');
        }

        if (player.energy < 20) {
            return interaction.reply('You do not have enough energy to heal!');
        }

        // Deduct energy cost for healing
        player.energy -= 20;

        const healAmount = player.maxHitPoints * 0.4;
        player.hitPoints += healAmount;
        if (player.hitPoints > player.maxHitPoints) {
            player.hitPoints = player.maxHitPoints;
        }

        // AI's turn to attack after the player heals
        const aiAttack = Math.floor(Math.random() * (enemy.attackRating + 1));
        player.hitPoints -= aiAttack;

        if (player.hitPoints <= 0) {
            player.battleState = false;
            player.hitPoints = player.maxHitPoints;
            await player.save();
            return interaction.reply(`After healing, you were attacked by the AI for ${aiAttack} damage and were defeated! Your adventure has ended. Use \`/adv\` to try again.`);
        }

        await player.save();
        return interaction.reply(`You healed for ${healAmount.toFixed(2)} HP. The AI attacked you for ${aiAttack} damage. Your HP: ${player.hitPoints.toFixed(2)}. Energy left: ${player.energy}`);
    },
};
