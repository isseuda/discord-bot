const { Schema, model } = require('mongoose');

const playerSchema = new Schema({
    userId: {
        type: String,
        required: true,
    },
    hitPoints: {
        type: Number,
        default: 100,
    },
    maxHitPoints: {
        type: Number,
        default: 100,
    },
    battleState: {
        type: Boolean,
        default: false,
    },
    energy: {
        type: Number,
        default: 100,
    },
    attackRating: {
        type: Number,
        default: 10,
    },
    defenseRating: {
        type: Number,
        default: 5,
    },
    hitAccuracy: {
        type: Number,
        default: 0.8,
    },
    speed: {
        type: Number,
        default: 10,
    }
});

module.exports = model('Player', playerSchema);
