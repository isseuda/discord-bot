const { Schema, model } = require('mongoose');

const enemySchema = new Schema({
    userId: {
        type: String,
        required: true,
        unique: true,
    },
    hitPoints: {
        type: Number,
        default: 100,
    },
    maxHitPoints: {
        type: Number,
        default: 100,
    },
    attackRating: {
        type: Number,
        default: 20,
    },
    speed: {
        type: Number,
        default: 10,
    }
});

module.exports = model('Enemy', enemySchema);
